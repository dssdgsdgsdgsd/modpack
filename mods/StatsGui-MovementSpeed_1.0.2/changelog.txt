---------------------------------------------------------------------------------------------------
Version: 1.0.2
Date: 2023-10-02
  Bugfixes:
    - Update changelog.
---------------------------------------------------------------------------------------------------
Version: 1.0.1
Date: 2023-10-02
  Bugfixes:
    - Update locale.
---------------------------------------------------------------------------------------------------
Version: 1.0.0
Date: 06. 05. 2022
  Features:
    - Initial release.