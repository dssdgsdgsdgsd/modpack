﻿using System;

namespace ModSettings;

public class ModSettings {
    public Version Version { get; set; }

    public ModSettingsContent Content { get; set; }
}